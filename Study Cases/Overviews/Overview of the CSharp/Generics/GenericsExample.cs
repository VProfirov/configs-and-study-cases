﻿namespace Overview_Advanced
{
    using System;
    public class GenericsExample
    {
        
    }

    public class Shelter<T>
    {
        private T shelterFor;
        private string[] animalShelterPlace = new string[100];
        public Shelter(T animal)
        {
            shelterFor = animal;
        }

        public string Name { get; set; }

        public void MakeNoise()
        {

        }

        public string this[int place]
        {
            get
            {
                if ( place > 0 && place <= animalShelterPlace.Length)
                {
                    return animalShelterPlace[place];
                }

                return "Error";
            }
            set
            {
                if (place > 0 && place <= animalShelterPlace.Length)
                {
                    animalShelterPlace[place] = value;
                }

            }
        }
    }
}