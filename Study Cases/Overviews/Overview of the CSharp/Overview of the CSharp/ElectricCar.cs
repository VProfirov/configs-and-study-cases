﻿using System;

namespace Overview_of_the_CSharp
{
    public class ElectricCar:Automobile,IElectricVehicle
    {
        public override void Start()
        {
            Console.WriteLine($"Start the car; Accelerate;");
        }

        public override void Stop()
        {
            Console.WriteLine($"Decelerate; Stop");
        }
    }
}