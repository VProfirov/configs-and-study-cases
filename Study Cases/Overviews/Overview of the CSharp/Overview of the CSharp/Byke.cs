﻿namespace Overview_of_the_CSharp
{
    public class Byke
    {
        public string Manufacturer { get; set; }
        public string Model { get; set; }
    }
}