﻿namespace Overview_of_the_CSharp
{
    public class VehicleRentalBusiness:IRental,IBusiness
    {
        public void OpenOperationalDay()
        {
        }

        public bool UserBackgroundValidation()
        {
            throw new System.NotImplementedException();
        }

        public bool DealCompletionProcedure()
        {
            throw new System.NotImplementedException();
        }

        public string MonthlyReport()
        {
            throw new System.NotImplementedException();
        }
    }

    public interface IBusiness
    {
        bool DealCompletionProcedure();
        string MonthlyReport();
    }

    public interface IRental
    {
        bool UserBackgroundValidation();
    }
}