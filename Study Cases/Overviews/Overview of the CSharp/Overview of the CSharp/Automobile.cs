﻿namespace Overview_of_the_CSharp
{
    public abstract class Automobile
    {
        public abstract void Start();
        public abstract void Stop();
        public string Manufacturer { get; set; }
        public string Model { get; set; }

        public decimal Price { get; set; }
    }
}
