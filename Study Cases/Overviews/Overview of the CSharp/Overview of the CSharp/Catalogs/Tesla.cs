﻿namespace Overview_of_the_CSharp.Catalogs
{
    public enum Tesla
    {
        Sportster = 0,
        ModelS = 1,
        ModelX = 2,
        ModelE = 3,
    }
}