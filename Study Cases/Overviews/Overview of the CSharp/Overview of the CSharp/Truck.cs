﻿using System;

namespace Overview_of_the_CSharp
{
    public class Truck:Automobile
    {
        public override void Start()
        {
            Console.WriteLine($"Start the engine; Shift to 1st; Release the break; Accelerate");
        }

        public override void Stop()
        {
            Console.WriteLine($"Decelerate; Stop; Put the berak on; Stop the engine");
        }
    }
}